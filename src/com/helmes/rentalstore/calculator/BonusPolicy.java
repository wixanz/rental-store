package com.helmes.rentalstore.calculator;

public interface BonusPolicy {

    int getBonus(int currentBonus);
}
