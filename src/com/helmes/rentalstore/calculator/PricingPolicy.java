package com.helmes.rentalstore.calculator;

import java.math.BigDecimal;

public interface PricingPolicy {

    BigDecimal getPrice(int days);
}
