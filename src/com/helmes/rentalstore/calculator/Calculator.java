package com.helmes.rentalstore.calculator;

import com.helmes.rentalstore.item.ItemType;

import java.math.BigDecimal;

public class Calculator {

    public static final double PREMIUM_FEE = 40;
    public static final double REGULAR_FEE = 30;
    private static final PricingPolicy NEW_RELEASE_PRICING = new PerDiemPricingPolicy(PREMIUM_FEE);
    private static final PricingPolicy DEFAULT_PRICING_POLICY = new PerDiemPricingPolicy(REGULAR_FEE);
    private static final PricingPolicy RELEASE_PRICING = new DiscountedPricingPolicy(1, NEW_RELEASE_PRICING);
    private static final PricingPolicy REGULAR_PRICING = new DiscountedPricingPolicy(3, DEFAULT_PRICING_POLICY);
    private static final PricingPolicy OLD_PRICING = new DiscountedPricingPolicy(5, DEFAULT_PRICING_POLICY);

    public static final int BONUS_POINTS_FOR_NEW_RELEASE = 2;
    public static final int BONUS_POINTS_FOR_OTHER = 1;
    private static final BonusPolicy RELEASE_BONUS_CHARGING = new TypedBonusPolicy(BONUS_POINTS_FOR_NEW_RELEASE);
    private static final BonusPolicy OTHER_BONUS_CHARGING = new TypedBonusPolicy(BONUS_POINTS_FOR_OTHER);


    public static BigDecimal calculatePrice(ItemType type, int days) {
        PricingPolicy pricingPolicy = getPricingPolicy(type);
        return pricingPolicy != null ? pricingPolicy.getPrice(days) : BigDecimal.ZERO;
    }

    public static Integer calculateChargedBonus(int customerBonus, ItemType type) {
        BonusPolicy bonusPolicy = getBonusPolicy(type);
        return bonusPolicy != null ? bonusPolicy.getBonus(customerBonus) : 0;
    }

    private static PricingPolicy getPricingPolicy(ItemType type) {
        switch (type) {
            case NEW_RELEASES:
                return RELEASE_PRICING;
            case REGULAR:
                return REGULAR_PRICING;
            case OLD:
                return OLD_PRICING;
        }
        return null;
    }

    private static BonusPolicy getBonusPolicy(ItemType type) {
        switch (type) {
            case NEW_RELEASES:
                return RELEASE_BONUS_CHARGING;
            default:
                return OTHER_BONUS_CHARGING;
        }
    }
}
