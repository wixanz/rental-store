package com.helmes.rentalstore.item;

import com.helmes.rentalstore.resource.Resource;
import com.helmes.rentalstore.resource.ResourceBundleType;

public class Video extends Item {

    public Video(String title, ItemType type) {
        super(title, type);
    }

    @Override
    public String toString() {
        return Resource.getInstance(ResourceBundleType.LABELS.getBundle()).getString("video.toString", getTitle(), getType()) +
                (isAvailable() ?
                        Resource.getInstance(ResourceBundleType.LABELS.getBundle()).getString("available") :
                        Resource.getInstance(ResourceBundleType.LABELS.getBundle()).getString("not.available"));
    }
}
