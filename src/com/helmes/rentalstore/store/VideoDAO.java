package com.helmes.rentalstore.store;

import com.helmes.rentalstore.item.ItemType;
import com.helmes.rentalstore.item.Video;

import java.util.List;

public interface VideoDAO {

    void addVideo(String title, ItemType type);

    void removeVideo(Video existedVideo);

    void changeVideoType(Video video, ItemType type);

    List<Video> getVideos();

    List<Video> getAvailableVideos();
}
