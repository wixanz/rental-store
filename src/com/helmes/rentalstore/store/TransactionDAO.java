package com.helmes.rentalstore.store;

import com.helmes.rentalstore.item.Video;
import com.helmes.rentalstore.staff.Customer;
import com.helmes.rentalstore.transaction.RentalTransaction;

import java.util.List;

public interface TransactionDAO {

    void rentVideo(Customer customer, Video existedVideo, int days, boolean useBonus);

    List<RentalTransaction> getRentalTransactions();

    List<RentalTransaction> getRentalTransactionsForCustomer(String name);
}
