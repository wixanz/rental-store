package com.helmes.rentalstore.store;

import com.helmes.rentalstore.item.ItemType;
import com.helmes.rentalstore.item.Video;
import com.helmes.rentalstore.resource.Resource;
import com.helmes.rentalstore.resource.ResourceBundleType;
import com.helmes.rentalstore.staff.Customer;
import com.helmes.rentalstore.transaction.RentalTransaction;

import java.text.MessageFormat;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class VideoStore implements VideoDAO, TransactionDAO {

    private static String[] fakeVideoTitles = {"Shrek", "Spider Man III", "Game of Thrones", "Mortal Combat"};
    private static String[] fakeCustomerNames = {"Kauri", "Lauri"};

    private List<Video> videos;
    private List<RentalTransaction> transactions;

    public VideoStore() {
        videos = new LinkedList<>();
        transactions = new LinkedList<>();

        generateFakeVideos();
        generateFakeTransactions();
    }

    private void generateFakeVideos() {
        System.out.println(Resource.getInstance(ResourceBundleType.LABELS.getBundle()).getString("fake.videos.generation"));

        for (String videoTitle : fakeVideoTitles)
            addVideo(videoTitle, ItemType.getRandom());

        System.out.println(Resource.getInstance(ResourceBundleType.LABELS.getBundle()).getString("end.of.fake.videos.generation"));
    }

    private void generateFakeTransactions() {
        System.out.println(Resource.getInstance(ResourceBundleType.LABELS.getBundle()).getString("fake.transactions.generation"));

        for (String customerName : fakeCustomerNames) {

            // Range: 1 - 31 days
            int randomDays = new Random().nextInt(30) + 1;
            boolean useBonusPoints = new Random().nextBoolean();
            Video randomVideo = getAvailableVideos().get(new Random().nextInt(videos.size() - 1));
            Customer customer = new Customer(customerName, 1000);

            rentVideo(customer, randomVideo, randomDays, useBonusPoints);
        }
        System.out.println(Resource.getInstance(ResourceBundleType.LABELS.getBundle()).getString("end.of.fake.transactions.generation"));
    }

    @Override
    public void addVideo(String title, ItemType type) {
        Video newVideo = new Video(title, type);
        videos.add(newVideo);
        System.out.println(Resource.getInstance(ResourceBundleType.LABELS.getBundle()).getString("added.video", newVideo.toString()));
    }

    @Override
    public void removeVideo(Video existedVideo) {
        videos.remove(existedVideo);
        System.out.println(Resource.getInstance(ResourceBundleType.LABELS.getBundle()).getString("removed.video", existedVideo.toString()));
    }

    @Override
    public void changeVideoType(Video video, ItemType type) {
        video.setType(type);
        System.out.println(Resource.getInstance(ResourceBundleType.LABELS.getBundle()).getString("video.change.type", video.getTitle(), video.getType()));
    }

    @Override
    public List<Video> getVideos() {
        return videos;
    }

    @Override
    public List<Video> getAvailableVideos() {
        List<Video> availableVideos = new LinkedList<>();

        for (Video video : videos) {
            if (video.isAvailable())
                availableVideos.add(video);
        }

        return availableVideos;
    }

    @Override
    public void rentVideo(Customer customer, Video existedVideo, int days, boolean useBonus) {

        // Change already rented video availability to False
        for (Video video : videos)
            if (video.equals(existedVideo)) {
                video.setAvailability(false);
                existedVideo = video;
            }

        RentalTransaction rentalTransaction;
        if (useBonus) {
            rentalTransaction = new RentalTransaction(customer, existedVideo, days, true);
            transactions.add(rentalTransaction);
        } else {
            rentalTransaction = new RentalTransaction(customer, existedVideo, days, false);
            transactions.add(rentalTransaction);
        }

        System.out.println(rentalTransaction.toString());
    }

    @Override
    public List<RentalTransaction> getRentalTransactions() {
        return transactions;
    }

    @Override
    public List<RentalTransaction> getRentalTransactionsForCustomer(String name) {
        List<RentalTransaction> customerTransactions = new LinkedList<>();

        for (RentalTransaction transaction : transactions)
            if (transaction.getCustomer().getName().equals(name))
                customerTransactions.add(transaction);

        return customerTransactions;
    }

    public Customer getCustomer(String name) {
        for (RentalTransaction transaction : transactions)
            if (transaction.getCustomer().getName().equals(name))
                return transaction.getCustomer();

        return null;
    }
}
