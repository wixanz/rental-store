package com.helmes.rentalstore;

import com.helmes.rentalstore.command.*;
import com.helmes.rentalstore.resource.Resource;
import com.helmes.rentalstore.resource.ResourceBundleType;
import com.helmes.rentalstore.store.VideoStore;
import com.helmes.rentalstore.util.Parser;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        // Initialize instances
        VideoStore store = new VideoStore();
        Executor executor = new Executor();
        Command command = null;

        Scanner scanner = new Scanner(System.in);
        System.out.println(Resource.getInstance(ResourceBundleType.LABELS.getBundle()).getString("welcome", 1));

        showCommands();

        while (true) {

            System.out.println(Resource.getInstance(ResourceBundleType.LABELS.getBundle()).getString("enter.command"));
            switch (Parser.tryParseToInteger(scanner.nextLine())) {
                case 1:
                    command = new AddVideo(store);
                    break;
                case 2:
                    command = new RemoveVideo(store);
                    break;
                case 3:
                    command = new ChangeVideoType(store);
                    break;
                case 4:
                    command = new GetVideos(store);
                    break;
                case 5:
                    command = new GetAvailableVideos(store);
                    break;
                case 6:
                    command = new RentVideo(store);
                    break;
                case 7:
                    command = new GetRentals(store);
                    break;
                case 8:
                    command = new GetRentalsForCustomer(store);
                    break;
                case 9:
                    System.exit(0);
                    break;
                default:
                    command = null;
                    System.out.println(Resource.getInstance(ResourceBundleType.ERRORS.getBundle()).getString("no.command"));
            }

            if (command != null)
                executor.run(command);
        }
    }


    private static void showCommands() {
        System.out.println(Resource.getInstance(ResourceBundleType.LABELS.getBundle()).getString("commands"));
        for (CommandType commandType : CommandType.values())
            System.out.println(commandType.toString());
        System.out.println(Resource.getInstance(ResourceBundleType.LABELS.getBundle()).getString("end"));
    }
}
