package com.helmes.rentalstore.util;

import com.helmes.rentalstore.resource.Resource;
import com.helmes.rentalstore.resource.ResourceBundleType;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Locale;

public class Converter {
    public static String ToDays(int days) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(days);
        if (days > 1)
            stringBuilder.append(Resource.getInstance(ResourceBundleType.LABELS.getBundle()).getString("days"));
        else
            stringBuilder.append(Resource.getInstance(ResourceBundleType.LABELS.getBundle()).getString("day"));
        return stringBuilder.toString();
    }

    public static String ToCurrency(BigDecimal price) {
        Currency currency = Currency.getInstance(Locale.getDefault());
        return currency.getSymbol() + price;
    }
}
