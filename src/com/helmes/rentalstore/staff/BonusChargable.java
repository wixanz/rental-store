package com.helmes.rentalstore.staff;

import com.helmes.rentalstore.item.ItemType;

public interface BonusChargable {

    void chargeBonus(ItemType type);

    void dischargeBonus(int bonusPoints);
}
