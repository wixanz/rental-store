package com.helmes.rentalstore.command;

import com.helmes.rentalstore.resource.Resource;
import com.helmes.rentalstore.resource.ResourceBundleType;

public enum CommandType {
    ADD_VIDEO(1, AddVideo.class.getSimpleName()),
    REMOVE_VIDEO(2, RemoveVideo.class.getSimpleName()),
    CHANGE_VIDEO_TYPE(3, ChangeVideoType.class.getSimpleName()),
    GET_VIDEOS(4, GetVideos.class.getSimpleName()),
    GET_AVAILABLE_VIDEOS(5, GetAvailableVideos.class.getSimpleName()),
    RENT_VIDEO(6, RentVideo.class.getSimpleName()),
    GET_RENTALS(7, GetRentals.class.getSimpleName()),
    GET_RENTALS_FOR_CUSTOMER(8, GetRentalsForCustomer.class.getSimpleName()),
    EXIT(9, Resource.getInstance(ResourceBundleType.LABELS.getBundle()).getString("exit"));

    private final Integer valueID;
    private final String value;

    private CommandType(Integer valueID, String value) {
        this.valueID = valueID;
        this.value = value;
    }

    @Override

    public String toString() {
        String presentableValue = value.replaceAll("([a-z]+)([A-Z])", "$1 $2");
        return valueID + " - " + (presentableValue.substring(0, 1).toUpperCase() + presentableValue.substring(1));
    }
}
