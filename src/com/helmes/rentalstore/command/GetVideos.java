package com.helmes.rentalstore.command;

import com.helmes.rentalstore.item.Video;
import com.helmes.rentalstore.resource.Resource;
import com.helmes.rentalstore.resource.ResourceBundleType;
import com.helmes.rentalstore.store.VideoStore;

public class GetVideos implements Command {
    private VideoStore store;

    public GetVideos(VideoStore store) {
        this.store = store;
    }

    @Override
    public void execute() {
        System.out.println(Resource.getInstance(ResourceBundleType.LABELS.getBundle()).getString("videos"));
        for (Video video : store.getVideos())
            System.out.println(video.toString());
    }
}
