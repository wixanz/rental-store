package com.helmes.rentalstore.command;

import com.helmes.rentalstore.item.ItemType;
import com.helmes.rentalstore.item.Video;
import com.helmes.rentalstore.resource.Resource;
import com.helmes.rentalstore.resource.ResourceBundleType;
import com.helmes.rentalstore.store.VideoStore;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class AddVideo implements Command {

    private VideoStore store;

    public AddVideo(VideoStore store) {
        this.store = store;
    }

    @Override
    public void execute() {
        Scanner scanner = new Scanner(System.in);

        String title = null;
        ItemType type = null;

        while (title == null || type == null) {

            if (title == null) {
                System.out.println(Resource.getInstance(ResourceBundleType.LABELS.getBundle()).getString("enter.video.title"));
                title = scanner.nextLine();

                for (Video existedVideo : store.getVideos())
                    if (existedVideo.getTitle().equals(title) || title.isEmpty()) {
                        title = null;
                        break;
                    }
            }

            if (title == null)
                System.out.println(Resource.getInstance(ResourceBundleType.ERRORS.getBundle()).getString("video.title.exists"));

            if (title != null) {

                List<ItemType> itemTypes = Arrays.asList(ItemType.values());

                System.out.println(Resource.getInstance(ResourceBundleType.LABELS.getBundle()).getString("enter.new.video.type", itemTypes));
                String tempType = scanner.nextLine().toUpperCase();

                for (ItemType value : itemTypes) {
                    if (value.toString().toUpperCase().equals(tempType)) {
                        type = value;
                        break;
                    }
                }
            }

            if (title != null && type == null)
                System.out.println(Resource.getInstance(ResourceBundleType.ERRORS.getBundle()).getString("wrong.video.type"));
        }

        store.addVideo(title, type);
    }
}
