package com.helmes.rentalstore.command;

import com.helmes.rentalstore.resource.Resource;
import com.helmes.rentalstore.resource.ResourceBundleType;
import com.helmes.rentalstore.store.VideoStore;
import com.helmes.rentalstore.transaction.RentalTransaction;

public class GetRentals implements Command {
    private VideoStore store;

    public GetRentals(VideoStore store) {
        this.store = store;
    }

    @Override
    public void execute() {
        System.out.println(Resource.getInstance(ResourceBundleType.LABELS.getBundle()).getString("rental.transactions"));
        for (RentalTransaction rentalTransaction : store.getRentalTransactions())
            System.out.println(rentalTransaction.toString());
    }
}
