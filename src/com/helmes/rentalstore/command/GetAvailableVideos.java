package com.helmes.rentalstore.command;

import com.helmes.rentalstore.item.Video;
import com.helmes.rentalstore.resource.Resource;
import com.helmes.rentalstore.resource.ResourceBundleType;
import com.helmes.rentalstore.store.VideoStore;

public class GetAvailableVideos implements Command {
    private VideoStore store;

    public GetAvailableVideos(VideoStore store) {
        this.store = store;
    }

    @Override
    public void execute() {
        System.out.println(Resource.getInstance(ResourceBundleType.LABELS.getBundle()).getString("available.videos"));
        for (Video video : store.getAvailableVideos())
            System.out.println(video.toString());
    }
}
