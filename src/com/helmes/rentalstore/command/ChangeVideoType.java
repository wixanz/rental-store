package com.helmes.rentalstore.command;

import com.helmes.rentalstore.item.ItemType;
import com.helmes.rentalstore.item.Video;
import com.helmes.rentalstore.resource.Resource;
import com.helmes.rentalstore.resource.ResourceBundleType;
import com.helmes.rentalstore.store.VideoStore;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class ChangeVideoType implements Command {
    private VideoStore store;

    public ChangeVideoType(VideoStore store) {
        this.store = store;
    }

    @Override
    public void execute() {
        Scanner scanner = new Scanner(System.in);
        Video existedVideo = null;
        ItemType newType = null;

        List<Video> videos = store.getVideos();

        while (existedVideo == null || newType == null) {

            if (existedVideo == null) {
                System.out.println(Resource.getInstance(ResourceBundleType.LABELS.getBundle()).getString("enter.video.title"));
                String tempTitle = scanner.nextLine();

                for (Video video : videos) {
                    if (video.getTitle().equals(tempTitle) && video.isAvailable()) {
                        existedVideo = video;
                        break;
                    }
                }
            }

            if (existedVideo == null)
                System.out.println(Resource.getInstance(ResourceBundleType.ERRORS.getBundle()).getString("cannot.change.video.properties"));

            if (existedVideo != null) {
                List<ItemType> itemTypes = Arrays.asList(ItemType.values());

                System.out.println(Resource.getInstance(ResourceBundleType.LABELS.getBundle()).getString("enter.new.video.type", itemTypes));
                String tempType = scanner.nextLine().toUpperCase();

                for (ItemType value : itemTypes) {
                    if (value.toString().toUpperCase().equals(tempType)) {
                        newType = value;
                        break;
                    }
                }
            }

            if (existedVideo != null && newType == null)
                System.out.println(Resource.getInstance(ResourceBundleType.ERRORS.getBundle()).getString("wrong.video.type"));

        }

        if (existedVideo != null && newType != null && existedVideo.getType().equals(newType))
            System.out.println(Resource.getInstance(ResourceBundleType.ERRORS.getBundle()).getString("video.types.identical"));
        else
            store.changeVideoType(existedVideo, newType);
    }
}
