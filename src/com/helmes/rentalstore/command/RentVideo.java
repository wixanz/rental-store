package com.helmes.rentalstore.command;

import com.helmes.rentalstore.calculator.Calculator;
import com.helmes.rentalstore.item.ItemType;
import com.helmes.rentalstore.item.Video;
import com.helmes.rentalstore.resource.Resource;
import com.helmes.rentalstore.resource.ResourceBundleType;
import com.helmes.rentalstore.staff.Customer;
import com.helmes.rentalstore.store.VideoStore;
import com.helmes.rentalstore.transaction.RentalTransaction;
import com.helmes.rentalstore.util.Converter;
import com.helmes.rentalstore.util.Parser;

import java.text.MessageFormat;
import java.util.Scanner;

public class RentVideo implements Command {

    private int selectedDays;
    private VideoStore store;
    private String customerName;
    private String videoTitle;
    private Boolean termsAcceptance;
    private Boolean useBonusPoints;
    private Customer customer;
    private Video selectedVideo;

    public RentVideo(VideoStore store) {
        this.store = store;
        this.customerName = null;
        this.videoTitle = null;
        this.selectedDays = 0;
        this.termsAcceptance = false;
        this.useBonusPoints = false;
        this.customer = null;
        this.selectedVideo = null;
    }

    @Override
    public void execute() {
        Scanner scanner = new Scanner(System.in);

        while (selectedVideo == null || selectedDays < 1) {

            if (customer == null) {
                System.out.println(Resource.getInstance(ResourceBundleType.LABELS.getBundle()).getString("enter.your.name"));
                customerName = scanner.nextLine();

                customer = findOrCreateNewCustomer(customerName);
            }

            if (customer != null && selectedVideo == null) {

                System.out.println(Resource.getInstance(ResourceBundleType.LABELS.getBundle()).getString("enter.video.title"));
                videoTitle = scanner.nextLine();

                selectedVideo = findVideo(videoTitle);
            }

            if (customer != null && selectedVideo != null && selectedDays < 1) {

                System.out.println(Resource.getInstance(ResourceBundleType.LABELS.getBundle()).getString("enter.rent.days"));
                selectedDays = Parser.tryParseToInteger(scanner.nextLine());
            }

            if (selectedVideo == null)
                System.out.println(Resource.getInstance(ResourceBundleType.ERRORS.getBundle()).getString("video.not.existed.or.rented"));

            if (selectedVideo != null && selectedDays < 1)
                System.out.println(Resource.getInstance(ResourceBundleType.ERRORS.getBundle()).getString("days.less.one"));
        }


        System.out.println(Resource.getInstance(ResourceBundleType.LABELS.getBundle()).getString("you.choose.video", selectedVideo.getTitle(), selectedVideo.getType(),
                Converter.ToDays(selectedDays), Converter.ToCurrency(Calculator.calculatePrice(selectedVideo.getType(), selectedDays))));


        if (selectedVideo.getType() == ItemType.NEW_RELEASES && customer.getBonus() > RentalTransaction.BONUS_CHARGE_PER_DIEM * selectedDays) {

            System.out.println(Resource.getInstance(ResourceBundleType.LABELS.getBundle()).getString("pay.by.bonus", customer.getBonus(),
                    RentalTransaction.BONUS_CHARGE_PER_DIEM * selectedDays));

            useBonusPoints = Boolean.parseBoolean(scanner.nextLine());

            if (useBonusPoints) {
                System.out.println(Resource.getInstance(ResourceBundleType.LABELS.getBundle()).getString("accept.terms"));
                termsAcceptance = Boolean.parseBoolean(scanner.nextLine());

                if (termsAcceptance)
                    store.rentVideo(customer, selectedVideo, selectedDays, true);
            }
        } else {

            System.out.println(Resource.getInstance(ResourceBundleType.LABELS.getBundle()).getString("accept.terms"));
            termsAcceptance = Boolean.parseBoolean(scanner.nextLine());

            if (termsAcceptance)
                store.rentVideo(customer, selectedVideo, selectedDays, false);
        }

        if (!termsAcceptance)
            System.out.println(Resource.getInstance(ResourceBundleType.LABELS.getBundle()).getString("rejected.terms"));
    }

    private Video findVideo(String title) {
        Video existedVideo = null;
        if (!title.isEmpty())
            for (Video video : store.getAvailableVideos())
                if (video.getTitle().equals(title)) {
                    existedVideo = video;
                    break;
                }
        return existedVideo;
    }

    private Customer findOrCreateNewCustomer(String customerName) {
        if (!customerName.isEmpty()) {
            Customer customer = store.getCustomer(customerName);
            if (customer == null) {
                customer = new Customer(customerName);
                System.out.println(Resource.getInstance(ResourceBundleType.LABELS.getBundle()).getString("new.customer.created", customer.getName()));
                return customer;
            } else
                return customer;
        }
        return null;
    }
}

