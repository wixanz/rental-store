package com.helmes.rentalstore.command;


import com.helmes.rentalstore.item.Video;
import com.helmes.rentalstore.resource.Resource;
import com.helmes.rentalstore.resource.ResourceBundleType;
import com.helmes.rentalstore.store.VideoStore;

import java.util.List;
import java.util.Scanner;

public class RemoveVideo implements Command {
    private VideoStore store;

    public RemoveVideo(VideoStore store) {
        this.store = store;
    }

    @Override
    public void execute() {
        Scanner scanner = new Scanner(System.in);
        Video existedVideo = null;

        List<Video> videos = store.getVideos();

        System.out.println(Resource.getInstance(ResourceBundleType.LABELS.getBundle()).getString("select.video.title"));
        for (Video video : videos)
            System.out.println(video.toString());

        while (existedVideo == null) {

            System.out.println(Resource.getInstance(ResourceBundleType.LABELS.getBundle()).getString("enter.video.title"));
            String tempTitle = scanner.nextLine();

            for (Video video : videos) {
                if (video.getTitle().equals(tempTitle)) {
                    existedVideo = video;
                    break;
                }
            }

            if (existedVideo == null)
                System.out.println(Resource.getInstance(ResourceBundleType.ERRORS.getBundle()).getString("no.such.video"));
        }

        store.removeVideo(existedVideo);
    }
}
