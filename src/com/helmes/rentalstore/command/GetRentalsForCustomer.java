package com.helmes.rentalstore.command;

import com.helmes.rentalstore.resource.Resource;
import com.helmes.rentalstore.resource.ResourceBundleType;
import com.helmes.rentalstore.store.VideoStore;
import com.helmes.rentalstore.transaction.RentalTransaction;

import java.text.MessageFormat;
import java.util.List;
import java.util.Scanner;

public class GetRentalsForCustomer implements Command {
    private VideoStore store;

    public GetRentalsForCustomer(VideoStore store) {
        this.store = store;
    }

    @Override
    public void execute() {

        Scanner scanner = new Scanner(System.in);

        System.out.println(Resource.getInstance(ResourceBundleType.LABELS.getBundle()).getString("enter.customer.name"));
        String name = scanner.nextLine();

        List<RentalTransaction> customerTransactions = store.getRentalTransactionsForCustomer(name);
        if (customerTransactions.size() > 0)
            for (RentalTransaction rentalTransaction : store.getRentalTransactionsForCustomer(name))
                System.out.println(rentalTransaction.toString());
        else
            System.out.println(Resource.getInstance(ResourceBundleType.ERRORS.getBundle()).getString("no.customer.transactions", name));
    }
}
