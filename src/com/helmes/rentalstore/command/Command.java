package com.helmes.rentalstore.command;

public interface Command {
    void execute();
}
