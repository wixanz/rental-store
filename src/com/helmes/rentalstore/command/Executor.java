package com.helmes.rentalstore.command;

public class Executor {

    public Executor() {
    }

    public void run(Command command) {
        command.execute();
    }
}
