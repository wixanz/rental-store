package com.helmes.rentalstore.store;

import com.helmes.rentalstore.item.ItemType;
import com.helmes.rentalstore.item.Video;
import com.helmes.rentalstore.resource.Resource;
import com.helmes.rentalstore.resource.ResourceBundleType;
import com.helmes.rentalstore.staff.Customer;
import com.helmes.rentalstore.transaction.RentalTransaction;
import org.junit.Before;
import org.junit.Test;

import java.text.MessageFormat;
import java.util.LinkedList;
import java.util.List;

import static junit.framework.Assert.*;

public class VideoStoreTest {

    private VideoStore videoStore;

    @Before
    public void setUp() throws Exception {
        /** We will check test based on predefined fake data **/
        videoStore = new VideoStore();
    }

    @Test
    public void testAddVideo() throws Exception {
        for (ItemType itemType : ItemType.values()) {
            String newTitle = "TestVideo" + itemType.name();
            videoStore.addVideo(newTitle, itemType);

            for (Video video : videoStore.getVideos()) {
                if (video.getTitle().equals(newTitle)) {
                    assertNotNull(video);
                    assertEquals(newTitle, video.getTitle());
                    break;
                }
            }
        }
    }

    @Test
    public void testRemoveVideo() throws Exception {
        Video videoToRemove = getFirstAvailableVideo();
        String videoToRemoveTitle = videoToRemove.getTitle();

        if (videoToRemove != null)
            videoStore.removeVideo(videoToRemove);

        if (!videoStore.getVideos().contains(videoToRemove))
            videoToRemove = null;

        assertNull(Resource.getInstance(ResourceBundleType.LABELS.getBundle()).getString("video.should.be.not.existed", videoToRemoveTitle), videoToRemove);
    }

    private Video getFirstAvailableVideo() {
        if (videoStore.getAvailableVideos().size() > 0)
            return videoStore.getAvailableVideos().get(0);

        return null;
    }

    @Test
    public void testChangeVideoType() throws Exception {
        Video videoToChange = getFirstAvailableVideo();
        ItemType newItemType = ItemType.NEW_RELEASES;

        if (videoToChange != null)
            videoStore.changeVideoType(videoToChange, newItemType);

        Video changedVideo = findVideo(videoToChange.getTitle());

        assertNotNull(changedVideo);
        assertEquals(newItemType, changedVideo.getType());
    }


    private Video findVideo(String title) {
        for (Video video : videoStore.getVideos()) {
            if (video.getTitle().equals(title)) {
                return video;
            }
        }

        return null;
    }

    @Test
    public void testGetVideos() throws Exception {
        assertNotNull(videoStore.getVideos());
        assertTrue(videoStore.getVideos().size() > 0);
    }

    @Test
    public void testGetAvailableVideos() throws Exception {
        assertNotNull(videoStore.getAvailableVideos());
        assertTrue(videoStore.getAvailableVideos().size() > 0);
    }

    @Test
    public void testRentVideo() throws Exception {
        checkRentVideo(10, 250, true);
        checkRentVideo(10, 1000, true);
        checkRentVideo(10, 1, false);
        checkRentVideo(10, 1000, false);

        /** Uncomment one of the line below to test rentVideo method with wrong parameters **/
//        checkRentVideo(10, 1, true);      // Expected result : Failed
//        checkRentVideo(0, 1, false);      // Expected result : Failed
    }

    private void checkRentVideo(int rentDays, int customerBonusPoints, boolean useBonus) {

        assertTrue(Resource.getInstance(ResourceBundleType.LABELS.getBundle()).getString("days.less.one"), rentDays > 0);

        Customer newCustomer = new Customer("TestCustomer" + customerBonusPoints, customerBonusPoints);
        Video video;

        if (useBonus) {
            assertTrue(rentDays * RentalTransaction.BONUS_CHARGE_PER_DIEM <= customerBonusPoints);
            video = getAvailableNewReleaseVideo();
            if (video != null) {
                assertEquals(ItemType.NEW_RELEASES, video.getType());
                videoStore.rentVideo(newCustomer, video, rentDays, useBonus);
            }
        } else {
            video = getFirstAvailableVideo();
            if (video != null)
                videoStore.rentVideo(newCustomer, video, rentDays, useBonus);
        }

        if (video != null) {
            for (RentalTransaction rentalTransaction : videoStore.getRentalTransactions()) {
                if (rentalTransaction.getCustomer().getName().equals(newCustomer.getName())) {
                    assertEquals(video.getTitle(), rentalTransaction.getItem().getTitle());
                    video.setAvailability(false);
                    assertEquals(video.isAvailable(), rentalTransaction.getItem().isAvailable());

                    if (useBonus) {
                        newCustomer.chargeBonus(rentalTransaction.getItem().getType());
                        assertEquals(newCustomer.getBonus(), rentalTransaction.getCustomer().getBonus());
                    }
                    break;
                }
            }
        }
    }

    private Video getAvailableNewReleaseVideo() {
        for (Video video : videoStore.getAvailableVideos())
            if (video.getType() == ItemType.NEW_RELEASES)
                return video;

        return null;
    }

    @Test
    public void testGetRentalTransactions() throws Exception {
        assertNotNull(videoStore.getRentalTransactions());
        assertTrue(videoStore.getRentalTransactions().size() > 0);
    }

    @Test
    public void testGetRentalTransactionsForCustomer() throws Exception {
        String name = "Kauri";
        List<RentalTransaction> customerTransactions = findCustomerRentalTransactions(name);

        assertNotNull(customerTransactions);
        assertEquals(1, customerTransactions.size());
    }

    private List<RentalTransaction> findCustomerRentalTransactions(String searchedName) {
        List<RentalTransaction> customerTransactions = new LinkedList<>();

        for (RentalTransaction transaction : videoStore.getRentalTransactions())
            if (transaction.getCustomer().getName().equals(searchedName))
                customerTransactions.add(transaction);

        return customerTransactions;
    }

    @Test
    public void testGetCustomer() throws Exception {
        String name = "Kauri";
        Customer customer = findCustomer(name);

        assertNotNull(customer);
        assertEquals(name, customer.getName());
    }

    private Customer findCustomer(String searchedName) {
        for (RentalTransaction transaction : videoStore.getRentalTransactions())
            if (transaction.getCustomer().getName().equals(searchedName))
                return transaction.getCustomer();

        return null;
    }
}