# Project: Video Rental Store #

This project was developed to demonstate OOP design principles, some pattern usage and Unit test coverage. 

### Development enviroment: ###
- Intellij IDEA
- JDK 1.8

### How to download the project? ###

* You can download this project following to Downloads -> Branches -> On the right side of 'master' row you will see .zip, .bz archive formats to download it.
* Moreover, you can just clone it via Git.